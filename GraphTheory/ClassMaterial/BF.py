__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph
from Common import INFINITY


def bellman_ford(g, w, s):
    assert isinstance(g, Graph)

    d = dict()
    pi = dict()

    for u in g.V:
        d[u] = INFINITY
        pi[u] = None
    d[s] = 0

    for i in range(0, len(g.V)-1):
        for u in g.V:
            for v in g.get_neighbors(u):
                if d[v] > d[u] + w(u, v):
                    d[v] = d[u] + w(u, v)
                    pi[v] = u

    for u in g.V:
        for v in u.get_neighbors(u):
            if d[v] > d[u] + w(u, v):
                return False

    return d, pi



