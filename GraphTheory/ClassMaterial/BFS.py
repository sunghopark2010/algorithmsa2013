from GraphTheory.ClassMaterial.Graph import Graph
from GraphTheory.ClassMaterial.Queue import Queue

__author__ = 'spark'

from Common import INFINITY, print_path


def bfs(g, s):
    """ Find the list of distances and shortest paths from a start vertex s to every vertex v in an unweighted graph G

    Args:
        g: an unweighted graph (can be both directed or undirected)
        s: a start vertex in g
    Returns:
        d: list of distances
        pi: list of parent nodes for vertices in the shortest paths
    Raises:
        ValueError: if s is not a vertex of g
    Running Time:
        O(V+E)

    """

    # check that g is a Graph object
    # check that s exists in g

    # initialize color, d and pi dictionaries
    # initialize q Queue

    # for each u in V
        # color[s] = white
        # d[u] = INFINITY
        # pi[u] = None

    # color[s] = gray
    # d[s] = 0
    # add s to q

    # while q is not empty
        # u = dequeue(q)
        # for each vertex v in adjacency of u
            # if color[u] == white
                # color[v] = gray
                # d[v] = d[u] + 1
                # pi[v] = v
                # add v to q
        # color[u] = black

    # check that g is a Graph object
    # check that s exists in g
    assert isinstance(g, Graph)
    if s not in g.V:
        raise ValueError('ERROR: %s does not exist in G.' % s)

    # initialize color, d and pi dictionaries
    # initialize q Queue
    d = {}
    color = {}
    pi = {}
    q = Queue()

    # for each u in V
        # color[s] = white
        # d[u] = INFINITY
        # pi[u] = None
    for u in g.V:
        d[u] = INFINITY
        color[u] = 'white'
        pi[u] = None

    # d[s] = 0
    # color[s] = gray
    # add s to q

    # color[s] = gray
    # d[s] = 0
    # add s to q
    color[s] = 'gray'
    d[s] = 0
    q.enqueue(s)

    # while q is not empty
        # u = dequeue(q)
        # for each vertex v in adjacency of u
            # if color[u] == white
                # color[v] = gray
                # d[v] = d[u] + 1
                # pi[v] = v
                # add v to q
        # color[u] = black
    while not q.is_empty():
        u = q.dequeue()
        for v in g.get_neighbors(u):
            if color[v] == 'white':
                color[v] = 'gray'
                d[v] = d[u] + 1
                pi[v] = u
                q.enqueue(v)
        color[u] = 'black'

    return d, pi


# example
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_vertex('f')
g.add_edge('a', 'b')
g.add_edge('a', 'd')
g.add_edge('b', 'a')
g.add_edge('b', 'c')
g.add_edge('c', 'b')
g.add_edge('c', 'd')
g.add_edge('c', 'f')
g.add_edge('d', 'a')
g.add_edge('d', 'c')
g.add_edge('d', 'e')
g.add_edge('d', 'f')
g.add_edge('e', 'd')
g.add_edge('e', 'f')
g.add_edge('f', 'c')
g.add_edge('f', 'd')
g.add_edge('f', 'e')
print g

d, pi = bfs(g, 'a')
print d
print pi

print_path('a', 'f', pi)