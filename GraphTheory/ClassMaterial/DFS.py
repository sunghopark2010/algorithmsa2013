from GraphTheory.ClassMaterial.Graph import Graph

__author__ = 'spark'

d = None
f = None
color = None
pi = None
time = None


def dfs(g):
    """ Find connected components of graph g

    Args:
        g: an unweighted graph (can be both directed or undirected)
    Returns:
        d: list of discovery times (the time the algorithm visits a vertex)
        f: list of finish times (the time the algorithm leaves a vertex)
        pi: list of parent nodes for vertices
    Raises:
        None
    Running Time:
        O(V+E)

    """

    # check that g is a Graph object

    # initialize, d, f, color and pi dictionaries

    # for each u in V
        # color[u] = white
        # pi[u] = None

    # time = 0

    # for each u in V
        # if color[u] == white
            # _dfs_visit(G, u)

    # def _dfs_visit(G, u)
        # time = time + 1
        # d[u] = time
        # color[u] = gray
        # for each v in adjacency of u
            # if color[v] == white
                # pi[v] = u
                # dfs_visit(G, v)
        # color[u] = black
        # time = time + 1
        # f[u] = time

    # check that g is a Graph object
    assert isinstance(g, Graph)

    # initialize, d, f, color and pi dictionaries
    global d, f, color, pi, time  # this is not a good programming style but I am following the course material
                                  # A better way would be to use a class
    d = dict()
    f = dict()
    color = dict()
    pi = dict()

    # for each u in V
        # color[u] = white
        # pi[u] = None
    for u in g.V:
        color[u] = 'white'
        pi[u] = None

    # time = 0
    time = 0

    # for each u in V
    # if color[u] == white
        # dfs_visit(G, u)
    for u in g.V:
        if color[u] == 'white':
            _dfs_visit(g, u)

    return d, f, pi


def _dfs_visit(g, u):
    global d, f, color, pi, time  # this is not a good programming style but I am following the course material
                                  # A better way would be to use a class
    # time = time + 1
    # d[u] = time
    # color[u] = gray
    time += 1
    d[u] = time
    color[u] = 'gray'

    # for each v in adjacency of u
        # if color[v] == white
            # pi[v] = u
            # dfs_visit(G, v)
    for v in g.get_neighbors(u):
        if color[v] == 'white':
            pi[v] = u
            _dfs_visit(g, v)

    # color[u] = black
    # time = time + 1
    # f[u] = time
    color[u] = 'black'
    time += 1
    f[u] = time

# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_vertex('f')
g.add_vertex('g')
g.add_vertex('h')
g.add_edge('a', 'b', directed_flg=False)
g.add_edge('a', 'e', directed_flg=False)
g.add_edge('c', 'd', directed_flg=False)
g.add_edge('c', 'g', directed_flg=False)
g.add_edge('c', 'h', directed_flg=False)
g.add_edge('d', 'h', directed_flg=False)
g.add_edge('g', 'h', directed_flg=False)

#d, f, pi = dfs(g)
#print d  # {'a': 1, 'c': 7, 'b': 2, 'e': 4, 'd': 8, 'g': 10, 'f': 15, 'h': 9}
#print f  # {'a': 6, 'c': 14, 'b': 3, 'e': 5, 'd': 13, 'g': 11, 'f': 16, 'h': 12}
#print pi  # {'a': None, 'c': None, 'b': 'a', 'e': 'a', 'd': 'c', 'g': 'h', 'f': None, 'h': 'd'}

'''
    Detection of a cycle for an undirected graph:
        Presence of a non-tree edge (backward edge; gray to gray)
    Types of edges for a directed graph
        Tree edges: part of a DFS forest; gray to white
        Back edges: from descendants to ancestors in a DFS tree; gray to gray
        Forward edges: lead to non-child descendants in a DFS tree; gray to black
        Cross edges: Lead neither to ancestors nor descendants in DFS tree; gray to black
    A directed graph has a cycle iff a dfs on G detects a back-edge.

'''



# algorithm to check a presence of cycle

color = None
cycle_found_flg = False


def is_cyclic(g):
    """ Return if g has a cycle or not

    Args:
        g: an unweighted graph (can be both directed or undirected)
    Returns:
        d: list of discovery times (the time the algorithm visits a vertex)
        f: list of finish times (the time the algorithm leaves a vertex)
        pi: list of parent nodes for vertices
    Raises:
        None
    Running Time:
        O(V+E)

    """

    # check that g is a Graph object

    # initialize color dictionary

    # for each u in V
        # color[u] = white

    # for each u in V
        # if color[u] == white
            # _is_cyclic_visit(g, u)

        # if cycle_found_flg:
            # return True

    # return False


    # def _is_cyclic_visit(G, u)
        # color[u] = gray
        # for each v in adjacency of u
            # if color[v] == white
                # _is_cyclic_visit(G, v)
            # if color[v] == gray
                # cycle_found_flg = True
        # color[u] = black


    # check that g is a Graph object
    assert isinstance(g, Graph)

    # initialize color dictionary
    global color, cycle_found_flg  # this is not a good programming style but I am following the course material
                  # A better way would be to use a class
    color = dict()
    cycle_found_flg = False

    # for each u in V
        # color[u] = white
    for u in g.V:
        color[u] = 'white'

    # for each u in V
        # if color[u] == white
            # _is_cyclic_visit(g, u)

        # if cycle_found_flg:
            # return True
    for u in g.V:
        if color[u] == 'white':
            _is_cyclic_visit(g, u)

        if cycle_found_flg:
            return True

    # return False
    return False


def _is_cyclic_visit(g, u):
    global color, cycle_found_flg  # this is not a good programming style but I am following the course material
                                   # A better way would be to use a class

    # color[u] = gray
    color[u] = 'gray'

    # for each v in adjacency of u
        # if color[v] == white
            # _is_cyclic_visit(G, v)
        # if color[v] == gray
            # cycle_found_flg = True
    for v in g.get_neighbors(u):
        if color[v] == 'white':
            _is_cyclic_visit(g, v)
        elif color[v] == 'gray':
            cycle_found_flg = True

    # color[u] = black
    color[u] = 'black'


# example run
# print g
# Graph:
# 	a: ['b', 'e']
# 	b: ['a']
#	c: ['d', 'g', 'h']
#	d: ['c', 'h']
#	e: ['a']
#	f: []
#	g: ['c', 'h']
#	h: ['c', 'd', 'g']

# print is_cyclic(g)
# True

# g2 = Graph()
# g2.add_vertex('a')
# g2.add_vertex('b')
# g2.add_vertex('c')
# g2.add_vertex('d')
# g2.add_vertex('e')
# g2.add_vertex('f')
# g2.add_edge('a', 'c')
# g2.add_edge('c', 'e')
# g2.add_edge('c', 'f')
# g2.add_edge('b', 'a')
# g2.add_edge('b', 'd')
# g2.add_edge('d', 'c')
# print g2
# Graph:
#	a: ['c']
#	b: ['a', 'd']
#	c: ['e', 'f']
#	d: ['c']
#	e: []
#	f: []

# print is_cyclic(g2)
# False