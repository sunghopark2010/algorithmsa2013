__author__ = 'spark'


class Graph(object):
    def __init__(self):
        self._adj_list = []
        self._weight_function = dict()

    @staticmethod
    def transpose(g):
        """ return a transposed graph of g

        Args:
            g: a Graph object
        Returns:
            gt: a transposed graph
        Raises:
            AssertionError: if g is not a Graph object

        """

        # assert that g is a Graph object

        # create gt a new Graph object

        # for u in g.V
            # add vertex to gt

        # for u in g.V
            # for v in adjacency of u
                # add edge from v to u in gt

        # return gt

        # assert that g is a Graph object
        assert isinstance(g, Graph)

        # create gt a new Graph object
        gt = Graph()

        # for u in g.V
            # add vertex to gt
        for u in g.V:
            gt.add_vertex(u)

        # for u in g.V
            # for v in adjacency of u
                # add edge from v to u in gt
        for u in g.V:
            for v in g.get_neighbors(u):
                gt.add_edge(v, u)

        # return gt
        return gt

    @property
    def V(self):
        """ return a list of vertices of this graph

        Args:
            self: this graph
        Returns:
            list of vertices in this graph
        Raises:
            None

        """

        # return list of vertices in this graph
        return map(lambda vertex: vertex.keys()[0], self._adj_list)

    def add_vertex(self, new_vertex):
        """ add a vertex to this Graph

        Args:
            self: this graph
            new_vertex: new vertex
        Returns:
            None
        Raises:
            ValueError: if new_vertex already exists in this Graph

        """

        # if new_vertex already exists
            # raise ValueError
        # else
            # add new_vertex to self._adj_list

        if new_vertex in self.V:
            raise ValueError("ERROR: %s is an already existing label in this graph." % new_vertex)
        else:
            self._adj_list.append({new_vertex: list()})

    def add_edge(self, start_vertex, dest_vertex, directed_flg=True):
        """ add an edge to this Graph

        Args:
            self: this graph
            start_vertex: label of the start vertex
            destination_vertex: label of the destination vertex
            directed_flg: indicating whether to create the edge in one or both direction
        Returns:
            none
        Raises:
            ValueError: if start_vertes and destination_vertex do not exist in this graph
            Warning: if an edge already exists between start_vertex and dest_vertex

        """

        # if start_vertex not in the list of vertices in this graph
            # raise ValueError
        # else if destination_vertex not in the list of vertices in this graph
            # raise ValueError
        # else
            # create an edge from start_vertex to destination_vertex
            # if not directed_flg
                # create an edge from destination_vertex to start_vertex

        if start_vertex not in self.V:
            raise ValueError("ERROR: %s does not exist in this graph." % start_vertex)
        elif dest_vertex not in self.V:
            raise ValueError("ERROR: %s does not exist in this graph." % dest_vertex)
        else:
            sv = self._get_vertex(start_vertex)
            if dest_vertex in sv[start_vertex]:
                raise Warning('WARNING: an edge between %s and %s already exists; nothing will happen.' % (start_vertex, dest_vertex))
            else:
                sv[start_vertex].append(dest_vertex)

            if not directed_flg:
                dv = self._get_vertex(dest_vertex)
                if start_vertex in dv[dest_vertex]:
                    raise Warning('WARNING: an edge between %s and %s already exists; nothing will happen.' %
                                  (dest_vertex, start_vertex))
                else:
                    dv[dest_vertex].append(start_vertex)

    def get_neighbors(self, vertex):
        """ return a list of vertices of this graph

        Args:
            self: this graph
            vertex: the vertex whose neighbors to get
        Returns:
            list of neighboring vertices of the vertex
        Raises:
            ValueError: if vertex is not in this graph

        """

        # if vertex is not in this graph
            # raise ValueError
        # else
            # return list of neighboring vertices of the vertex

        if vertex not in self.V:
            raise ValueError("ERROR: %s does not exist in this graph." % vertex)
        else:
            return self._get_neighbors(vertex)

    def remove_edge(self, u, v, directed_flg=True):
        """ remove an edge between u and v

        Args:
            self: this graph
            u: start vertex of an edge
            v: destination vertex of an edge
            directed_flg: flag indicating whether to remove the edge in both directions
        Returns:
            None
        Raises:
            ValueError: if u and v are not vertices of this graph
            Warning: if an edge between u and v does not exist

        """

        # if u is not in V
            # raise ValueError
        # if v is not in V
            # raise ValueError

        # remove edge between u and v
        # if directed_flg == False
            # remove edge between v and u

        # if u is not in V
            # raise ValueError
        # if v is not in V
            # raise ValueError
        if u not in self.V:
            raise ValueError("ERROR: %s does not exist in this graph." % u)
        if v not in self.V:
            raise ValueError("ERROR: %s does not exist in this graph." % v)

        # remove edge between u and v
        # if directed_flg == False
            # remove edge between v and u
        self._remove_edge(u, v)
        if not directed_flg:
            self._remove_edge(v, u)

    def _remove_edge(self, u, v):
        # get neighbors of u
        # if v not in the neighbors
            # raise Warning
        # else
            # remove v from u's neighbors
        neighbors = self.get_neighbors(u)
        if v not in neighbors:
            raise Warning('WARNING: an edge between %s and %s does not exist; nothing will happen.' % (u, v))
        else:
            self._get_vertex(u)[u].remove(v)

    def _get_vertex(self, vertex_label):
        return filter(lambda vertex: vertex.keys()[0] == vertex_label, self._adj_list)[0]

    def _get_neighbors(self, vertex_label):
        return self._get_vertex(vertex_label)[vertex_label]

    def __str__(self):
        ret_str = 'Graph:\n'
        for vertex in self._adj_list:
            ret_str += ('\t%s: %s\n' % (vertex.keys()[0], vertex[vertex.keys()[0]]))
        return ret_str