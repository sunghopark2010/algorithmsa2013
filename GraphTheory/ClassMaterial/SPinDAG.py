__author__ = 'spark'


# finding shortest paths for a dag that has negative weights
# find a topological order and run BF from a source vertex to a sink vertex
# running time: finding the topological order O(V+E), run BF O(V)

from GraphTheory.ClassMaterial.Graph import Graph
from GraphTheory.ClassMaterial.DAG import topological_sort
from Common import INFINITY

def find_sps_in_dag(g, w):
    assert isinstance(g, Graph)

    d = dict()
    pi = dict()

    for u in g.V:
        d[u] = INFINITY
        pi[u] = None

    l = topological_sort(g)
    d[l[0]] = 0

    for u in l:
        for v in g.get_neighbors(u):
            if d[v] > d[u] + w(u, v):
                d[v] = d[u] + w(u, v)
                pi[v] = u

    return d, pi

# example run
g = Graph()

g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_edge('a', 'b')
g.add_edge('b', 'c')
g.add_edge('a', 'c')
g.add_edge('b', 'e')
g.add_edge('b', 'd')
g.add_edge('e', 'd')
g.add_edge('d', 'c')


def weight_function(u, v):
    _weight_dict = dict()
    _weight_dict[('a', 'b')] = -1
    _weight_dict[('a', 'c')] = 4
    _weight_dict[('b', 'c')] = 3
    _weight_dict[('d', 'c')] = 5
    _weight_dict[('b', 'e')] = 2
    _weight_dict[('e', 'd')] = -3
    _weight_dict[('b', 'd')] = 2
    return _weight_dict[(u, v)]

d, pi = find_sps_in_dag(g, weight_function)
print d
print pi