__author__ = 'spark'

from Common import INFINITY


def print_path(s, v, pi):
    """ Print a path from s to v given we know parents of every vertex

    Args:
        s: start vertex
        v: destination vertex
        pi: dictionary showing the parent vertex of a vertex
    Returns:
        None; only print out the path
    Raises:
        ValueError: if s and/or v are not in the list of vertices in pi
    Running Time:
        O(V)

    """
    # check that pi is dict, and s and v are strings

    # if s does not exist in pi
        # raise ValueError
    # if v does not exist in pi
        # raise ValueError

    # if v == s:
        # print s
    # else if pi[v] == None:
        # print "No path"
    # else:
        # print_path(s, pi[v], pi)
        # print v

    assert isinstance(pi, dict)
    assert isinstance(s, str)
    assert isinstance(v, str)

    _print_path(s, v, pi)


def _print_path(s, v, pi):
    if s not in pi.keys():
        raise ValueError('ERROR: %s does not exist in pi.' % s)
    if v not in pi.keys():
        raise ValueError('ERROR: %s does not exist in pi.' % v)

    if v == s:
        print s
    elif not pi[v]:
        print 'No path'
    else:
        _print_path(s, pi[v], pi)
        print v