__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph
from GraphTheory.ClassMaterial.DFS import dfs, is_cyclic


def topological_sort(g, version='classroom'):
    """ Find the topological sort of g (i.e. find a list of vertices such that all the edges go from left to right)

    Args:
        g: a non-cyclic, directed, unweighted graph
        version: a string to indicate which version to run (classroom or my); both have the same algorithm
    Returns:
        l: list of vertices in topological order (i.e. decreasing order of finish time in DFS)
    Raises:
        ValueError: if g is cyclic or if version is neither 'classroom' nor 'my'
    Running Time:
        O(V+E)

    """

    # classroom version
    # Initialize l to an empty list
    # run dfs(g)
    # if a vertex is finished, add it to L
    # reverse l
    # return l

    # my version
    # run dfs(g)
    # return sorted vertices by decreasing order of f

    if version == 'classroom':
        return _topological_sort_classroom(g)
    elif version == 'my':
        return _topological_sort_my(g)
    else:
        raise ValueError("ERROR: %s is neither 'classroom' nor 'my'" % version)


def _topological_sort_my(g):
    # assert that g is a graph

    # if g is cyclic
        # raise ValueError

    # run dfs and get finish time
    # return sorted vertices by decreasing order of f


    # assert that g is a graph
    assert isinstance(g, Graph)

    # if g is cyclic
        # raise ValueError
    if is_cyclic(g):
        raise ValueError('ERROR: given graph is cyclic.')

    # run dfs and get finish time
    d, f, pi = dfs(g)

    # return sorted vertices by decreasing order of f
    return map(lambda sorted_vertex: sorted_vertex[0], sorted(f.items(), key=lambda vertex: vertex[1], reverse=True))


# variables for topological_sort_classroom
l = None
f = None
color = None


def _topological_sort_classroom(g):
    # assert that g is graph
    # initialize l to an empty list, and f and color to dictionaries

    # for each u in V
        # color[u] = white

    # for each u in V
        # if color[u] == white
            # _dfs_visit(G, u)

    # return l

    # def _dfs_visit(G, u)
        # color[u] = gray
        # for each v in adjacency of u
            # if color[v] == white
                # _topological_sort_classroom_visit(G, v)
        # color[u] = black
        # add u to l

    # assert that g is graph
    assert isinstance(g, Graph)

    # initialize l to an empty list, and f and color to dictionaries
    global l, f, color
    l = list()
    f = dict()
    color = dict()

    # for each u in V
        # color[u] = white
    for u in g.V:
        color[u] = 'white'

    # for each u in V
        # if color[u] == white
            # _topological_sort_classroom_visit(G, u)
    for u in g.V:
        if color[u] == 'white':
            _topological_sort_classroom_visit(g, u)

    # return l
    return l[::-1]


def _topological_sort_classroom_visit(g, u):
    global l, f, color

    # color[u] = gray
    color[u] = 'gray'

    # for each v in adjacency of u
        # if color[v] == white
            # _topological_sort_classroom_visit(G, v)
    for v in g.get_neighbors(u):
        if color[v] == 'white':
            _topological_sort_classroom_visit(g, v)

    # color[u] = black
    color[u] = 'black'

    # add u to l
    l.append(u)

# example run
'''
dag = Graph()
dag.add_vertex('a')
dag.add_vertex('b')
dag.add_vertex('c')
dag.add_vertex('d')
dag.add_vertex('e')
dag.add_vertex('f')
dag.add_edge('a', 'c')
dag.add_edge('c', 'e')
dag.add_edge('c', 'f')
dag.add_edge('b', 'a')
dag.add_edge('d', 'c')

print topological_sort(dag, 'my')
# ['d', 'b', 'a', 'c', 'f', 'e']
print topological_sort(dag, 'classroom')
# ['d', 'b', 'a', 'c', 'f', 'e']
'''

