from GraphTheory.ClassMaterial.Graph import Graph

__author__ = 'spark'

'''
    Key property: a source SCC in G^t is a sink SCC in G
                In other words, if run DFS on G^t, the vertex with the highest finish time is in a source SCC of G^t
                and thus in a sink SCC of G
'''

from GraphTheory.ClassMaterial.DAG import topological_sort

scc = None
num_scc = None
color = None


def dfs_scc(g):
    """ Find SCCs of g

    Args:
        g: an unweighted, directed graph g
    Returns:
        scc: a dictionary whose key is a vertex and item is an index of a SCC
    Raises:
        AssertionError: if g is not a Graph object
    Running Time:
        O(V+E)

    """
    # assert that g is a Graph object

    # construct Gt

    # get topological order of Gt

    # initialize scc to a dictionary
    # num_scc = 0

    # for each u in V:
        # color[u] = white

    # for each u in V in the topological order:
        # if color[u] == white
            # num_scc = num_scc + 1
            # _dfs_visit_scc(g, u)

    # def _dfs_visit_scc(g, u):
        # color[u] = 'non-white'
        # scc[u] = num_scc
        # for each v in adjacency of u:
            # if color[v] == white
                # _dfs_visit_scc(g, v)

    # return scc

    # assert that g is a Graph object
    assert isinstance(g, Graph)

    # construct Gt
    gt = Graph.transpose(g)

    # get topological order of Gt
    l = topological_sort(gt)

    # initialize scc and color to a dictionary
    # num_scc = 0
    global scc, num_scc, color
    scc = dict()
    num_scc = 0
    color = dict()

    # for each u in V:
        # color[u] = white
    for u in g.V:
        color[u] = 'white'

    # for each u in V in the topological order:
        # if color[u] == white
            # num_scc = num_scc + 1
            # _dfs_visit_scc(g, u)
    for u in l:
        if color[u] == 'white':
            num_scc += 1
            _dfs_visit_scc(g, u)

    # def _dfs_visit_scc(g, u):
        # color[u] = 'non-white'
        # scc[u] = num_scc
        # for each v in adjacency of u:
            # if color[v] == white
                # _dfs_visit_scc(g, v)

    # return scc
    return scc


def _dfs_visit_scc(g, u):
    # color[u] = 'non-white'
    # scc[u] = num_scc
    # for each v in adjacency of u:
        # if color[v] == white
            # _dfs_visit_scc(g, v)

    # color[u] = 'non-white'
    # scc[u] = num_scc
    global color, scc, num_scc
    color[u] = 'non_white'
    scc[u] = num_scc

    # for each v in adjacency of u:
        # if color[v] == white
            # _dfs_visit_scc(g, v)
    for v in g.get_neighbors(u):
        if color[v] == 'white':
            _dfs_visit_scc(g, v)

'''
# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_vertex('f')
g.add_edge('a', 'b')
g.add_edge('b', 'c')
g.add_edge('b', 'e', directed_flg=False)
g.add_edge('c', 'f', directed_flg=False)
g.add_edge('e', 'f')
g.add_edge('b', 'd')

print dfs_scc(g)
'''