__author__ = 'spark'


class Queue(object):
    def __init__(self):
        self._queue_list = list()

    def enqueue(self, item):
        """ insert an item to this queue

        Args:
            self: this queue
            item: item to be added to his queue
        Returns:
            None
        Raises:
            None

        """

        # add an item to this queue

        self._queue_list.append(item)

    def dequeue(self):
        """ get the first item in this queue

        Args:
            self: this queue
        Returns:
            the first item in this queue
        Raises:
            EmptyQueueError: if this queue is empty

        """

        # if this list is empty
            # raise EmptyQueueError
        # else
            # get the first item
            # remove it from the list
            # return the first item

        if not self._queue_list:
            class EmptyQueueError(Exception):
                pass
            raise EmptyQueueError('ERROR: this queue is empty.')
        else:
            first_item = self._queue_list[0]
            self._queue_list.remove(first_item)
            return first_item

    def get_queue(self):
        """ get the items in this queue

        Args:
            self: this queue
        Returns:
            the list of items in this queue
        Raises:
            None

        """

        # return the list of items in this queue

        return self._queue_list

    def is_empty(self):
        """ return whether this queue is empty or not

        Args:
            self: this queue
        Returns:
            True if this queue is empty; False otherwise
        Raises:
            None

        """

        # if this queue is not empty
            # return False
        # else
            # return True

        if self._queue_list:
            return False
        else:
            return True

    def __str__(self):
        return 'Queue: %s' % self._queue_list