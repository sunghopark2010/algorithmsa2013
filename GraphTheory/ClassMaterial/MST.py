__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph
from Common import INFINITY

# cut property
# if X is a part of MST, X U {e} is also a part of MST when e is a minimum-weight edge between two subsets of
# vertices S and V-S between which any edge in X does not cross.


def mst(g, w, version='Prim'):
    assert isinstance(g, Graph)

    # general mst
    # X starts with minimum edge in g  # O(E)
    # while |X| < |V| - 1
        # let S in V be a set such that no edge in X cross S, V-S
        # let e in E be a min-weight crossing S, V-S
        # X = X U {e}

    # Prim's and Kruskal's algorithms are two implementations of this general scheme

    # Prim's algorithm
    # Running time: O(V^2) for array implementation, O(ElgV) for binary min-heap implementation
    if version == 'Prim':

        pi = dict()
        color = dict()
        key = dict()
        q = dict()

        for u in g.V:
            pi[u] = None
            color[u] = 'white'
            key[u] = INFINITY
            q[u] = INFINITY

        s = g.V[0]
        key[s] = 0

        while q.keys():
            u = sorted(q.items(), key=lambda item: item[1])[0][0]
            q.pop(u)
            color[u] = 'black'
            for v in g.get_neighbors(u):
                if color[v] == 'white' and key[v] > w(u, v):
                    pi[v] = u
                    key[v] = w(u, v)
                    q[v] = key[v]

        return key, pi

    # Kriskal's algorithm
    # Running time: O(ElgV)






# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_vertex('f')

g.add_edge('a', 'b', directed_flg=False)
g.add_edge('b', 'c', directed_flg=False)
g.add_edge('a', 'c', directed_flg=False)
g.add_edge('a', 'd', directed_flg=False)
g.add_edge('b', 'd', directed_flg=False)
g.add_edge('c', 'e', directed_flg=False)
g.add_edge('c', 'f', directed_flg=False)
g.add_edge('c', 'd', directed_flg=False)
g.add_edge('e', 'f', directed_flg=False)


def weight_function(u, v):
    _weight_dict = dict()
    _weight_dict[('a', 'b')] = 5
    _weight_dict[('a', 'c')] = 6
    _weight_dict[('a', 'd')] = 4
    _weight_dict[('b', 'c')] = 1
    _weight_dict[('b', 'd')] = 2
    _weight_dict[('c', 'd')] = 2
    _weight_dict[('d', 'f')] = 4
    _weight_dict[('c', 'f')] = 3
    _weight_dict[('c', 'e')] = 5
    _weight_dict[('e', 'f')] = 4

    try:
        return _weight_dict[(u, v)]
    except KeyError:
        try:
            return _weight_dict[(v, u)]
        except KeyError:
            print 'Weight for the edge (%s, %s) not found.' % (u, v)


key, pi = mst(g, weight_function)
print key
print pi