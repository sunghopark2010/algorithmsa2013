from GraphTheory.ClassMaterial.Graph import Graph

__author__ = 'spark'

from Common import INFINITY

# global variables for the Dijkstra's algorithm
d = None
pi = None
S = None
q = None


def dijkstra(g, w, s):
    """ Find shortest paths from s to every vertex u in g

    Args:
        g: a Graph object
        w: a weight function
        s: a start vertex in g
    Returns:
        d: distance from s to a vertex
        pi: list to show parents
    Raises:
        AssertionError: if g is not a Graph object
        KeyError: if w does not have a weight for an edge in g
    Running Time:
        O((V+E)lgV)

    """
    # assert that g is a Graph object

    # initialize d and pi to dictionaries, s to a set and q to a list of g.V
    # for each u in V
        # d[u] = INFINITY
        # pi[u] = None
    # d[s] = 0

    # while q is not empty
        # u = extract_min(q)  # I didn't implement the data structure
        # S = S U {u}
        # for each v in adjacency of u
            # if d[v] > d[u] + w(u, v)
                # d[v] = d[u] + w(u, v)
                # pi[v] = u

    # return d, pi

    # assert that g is a Graph object
    assert isinstance(g, Graph)

    # initialize d and pi to dictionaries, s to a set and q to a list of g.V
    global d, pi, S, q
    d = dict()
    pi = dict()
    S = set()
    q = g.V
    assert isinstance(q, list)

    # for each u in V
        # d[u] = INFINITY
        # pi[u] = None
    # d[s] = 0
    for u in g.V:
        d[u] = INFINITY
        pi[u] = None
    d[s] = 0

    # while q is not empty
        # u = extract_min(q)  # I didn't implement the data structure
        # S = S U {u}
        # for each v in adjacency of u
            # if d[v] > d[u] + w(u, v)
                # d[v] = d[u] + w(u, v)
                # pi[v] = u
    while q:
        u = filter(lambda vertex: d[vertex] == min(map(lambda fd: fd[1], filter(lambda distance: distance[0] in q, d.items()))), q)[0]
        q.remove(u)
        S.add(u)
        for v in g.get_neighbors(u):
            if d[v] > d[u] + w(u, v):
                d[v] = d[u] + w(u, v)
                pi[v] = u

    # return d, pi
    return d, pi

'''
# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_vertex('f')

g.add_edge('a', 'b')
g.add_edge('b', 'f')
g.add_edge('c', 'b')
g.add_edge('a', 'c')
g.add_edge('c', 'f')
g.add_edge('a', 'd')
g.add_edge('d', 'c')
g.add_edge('d', 'e')
g.add_edge('c', 'e')
g.add_edge('e', 'f')


def weight_function(u, v):
    weight_dict = dict()
    weight_dict[('a', 'b')] = 4
    weight_dict[('b', 'f')] = 2
    weight_dict[('a', 'c')] = 2
    weight_dict[('c', 'b')] = 1
    weight_dict[('c', 'f')] = 4
    weight_dict[('a', 'd')] = 1
    weight_dict[('d', 'c')] = 1
    weight_dict[('d', 'e')] = 2
    weight_dict[('c', 'e')] = 3
    weight_dict[('e', 'f')] = 1

    return weight_dict[(u, v)]

d, pi =  dijkstra(g, weight_function, 'a')
print d
# {'a': 0, 'c': 2, 'b': 3, 'e': 3, 'd': 1, 'f': 4}
print pi
# {'a': None, 'c': 'a', 'b': 'c', 'e': 'd', 'd': 'a', 'f': 'e'}
print print_path('a', 'f', pi)
# a
# d
# e
# f
# None
'''