
__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph
from Common import INFINITY

# use Bellman-Ford


def bf_with_negative_weight_cycles(g, w):
    assert isinstance(g, Graph)

    d = dict()
    pi = dict()

    for u in g.V:
        d[u] = INFINITY
        pi[u] = None
    d[g.V[0]] = 0

    for i in range(0, len(g.V) - 1):
        for u in g.V:
            for v in g.get_neighbors(u):
                if d[v] > d[u] + w(u, v):
                    pi[v] = u
                    d[v] = d[u] + w(u, v)

    for u in g.V:
        for v in g.get_neighbors(u):
            if d[v] > d[u] + w(u, v):  # negative-weight cycle exists
                d[v] = -INFINITY
                pi[v] = None
                c = v

                # also mark vertices in the cycle O(V)
                while True:
                    if pi[c] not in d.keys() or d[pi[c]] == -INFINITY:
                        break
                    else:
                        d[pi[c]] = -INFINITY
                        pi[pi[c]] = None
                        c = pi[c]

    return d, pi


# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_vertex('f')

g.add_edge('a', 'b')
g.add_edge('a', 'e')
g.add_edge('e', 'b')
g.add_edge('b', 'c')
g.add_edge('f', 'b')
g.add_edge('e', 'f')
g.add_edge('f', 'c')
g.add_edge('c', 'd')
g.add_edge('d', 'f')


def weight_function(u, v):
    _weight_dict = dict()
    _weight_dict[('a', 'b')] = 1
    _weight_dict[('a', 'e')] = 2
    _weight_dict[('e', 'b')] = -2
    _weight_dict[('b', 'c')] = 3
    _weight_dict[('e', 'f')] = 3
    _weight_dict[('f', 'b')] = -6
    _weight_dict[('f', 'c')] = -4
    _weight_dict[('c', 'd')] = 2
    _weight_dict[('d', 'f')] = 2

    return _weight_dict[(u, v)]

# without negative-weight cycles
d, pi = bf_with_negative_weight_cycles(g, weight_function)
print d
# {'a': 0, 'c': 1, 'b': -1, 'e': 2, 'd': 3, 'f': 5}
print pi
# {'a': None, 'c': 'f', 'b': 'f', 'e': 'a', 'd': 'c', 'f': 'e'}

# with a negative-weight cycle


def weight_function2(u, v):
    _weight_dict = dict()
    _weight_dict[('a', 'b')] = 1
    _weight_dict[('a', 'e')] = 2
    _weight_dict[('e', 'b')] = -2
    _weight_dict[('b', 'c')] = 3
    _weight_dict[('e', 'f')] = 3
    _weight_dict[('f', 'b')] = -6
    _weight_dict[('f', 'c')] = -5
    _weight_dict[('c', 'd')] = 2
    _weight_dict[('d', 'f')] = 2

    return _weight_dict[(u, v)]
d, pi = bf_with_negative_weight_cycles(g, weight_function2)
print d
# {'a': 0, 'c': -9999, 'b': -9999, 'e': 2, 'd': -9999, 'f': -9999}
print pi
# {'a': None, 'c': None, 'b': None, 'e': 'a', 'd': None, 'f': None}






