__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph
from GraphTheory.ClassMaterial.DAG import topological_sort


def get_num_quarters(g):
    # idea: find sink nodes by running dfs on gt
    #       run dfs from sink nodes; assign index = index + 1
    #       update max_index while running dfs

    assert isinstance(g, Graph)

    gt = Graph.transpose(g)
    l = topological_sort(gt)  # O(V+E)

    index = dict()
    max_index = 0

    for u in l:
        print u
        neighbors = g.get_neighbors(u)
        if neighbors:
            current_max_index = 0
            for v in neighbors:
                if index[v] > current_max_index:
                    current_max_index = index[v]
                index[u] = current_max_index + 1
                if current_max_index > max_index:
                    max_index = current_max_index + 1
        else:
            index[u] = 1
            if index[u] > max_index:
                max_index = index[u]

    return max_index

# example run
g0 = Graph()
g0.add_vertex('a')
g0.add_vertex('b')
g0.add_vertex('c')
g0.add_vertex('d')
g0.add_edge('a', 'b')
g0.add_edge('b', 'c')
g0.add_edge('c', 'd')
print get_num_quarters(g0)
# 3

# example run
g = Graph()
g.add_vertex('a0')
g.add_vertex('a1')
g.add_vertex('b0')
g.add_vertex('b1')
g.add_vertex('b2')
g.add_vertex('b3')
g.add_vertex('c0')
g.add_vertex('c1')
g.add_vertex('c2')
g.add_vertex('d0')
g.add_vertex('d1')
g.add_vertex('d2')
g.add_edge('a0', 'b0')
g.add_edge('a0', 'b1')
g.add_edge('a1', 'b2')
g.add_edge('a1', 'b3')
g.add_edge('b1', 'c0')
g.add_edge('b1', 'c1')
g.add_edge('b2', 'c2')
g.add_edge('c0', 'd0')
g.add_edge('c0', 'd1')
g.add_edge('c0', 'd2')
print get_num_quarters(g)
# 3