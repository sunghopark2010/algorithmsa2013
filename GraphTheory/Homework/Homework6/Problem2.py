__author__ = 'spark'

from Common import INFINITY
from GraphTheory.ClassMaterial.Queue import Queue
from GraphTheory.ClassMaterial.Graph import Graph


def get_num_shortest_paths(g, u, w):
    """
        Often there are multiple shortest paths between two vertices in a graph. Give an O(V + E) time algorithm for the following task.
        Input: An undirected graph G = (V, E) in adjacency list format; vertices u and w in V.
        Output: The number of distinct shortest paths from u to w.
    """

    assert isinstance(g, Graph)

    color = dict()
    d = dict()
    pi = dict()
    num_sps = dict()
    q = Queue()

    for v in g.V:
        color[v] = 'white'
        d[v] = INFINITY
        pi[v] = None
        num_sps[v] = 0

    num_sps[u] = 1
    color[u] = 'gray'
    q.enqueue(u)

    while not q.is_empty():
        v = q.dequeue()
        for adj in g.get_neighbors(v):
            if color[adj] == 'white':
                d[adj] = d[v] + 1
                pi[adj] = v
                num_sps[adj] = num_sps[v]
                color[adj] = 'gray'
                q.enqueue(adj)
            elif color[adj] == 'gray':
                if d[v] + 1 == d[adj]:
                    num_sps[adj] += num_sps[pi[adj]]
        color[u] = 'black'
    return num_sps

# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_vertex('f')
g.add_vertex('g')
g.add_vertex('a1')
g.add_edge('a', 'b')
g.add_edge('a', 'c')
g.add_edge('b', 'd')
g.add_edge('c', 'd')
g.add_edge('d', 'e')
g.add_edge('d', 'f')
g.add_edge('f', 'g')
g.add_edge('e', 'g')
g.add_edge('a', 'a1')
g.add_edge('a1', 'd')
print g
print get_num_shortest_paths(g, 'a', 'd')