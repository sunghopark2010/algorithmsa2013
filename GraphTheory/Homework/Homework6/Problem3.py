__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph

# global variables
color = None
is_bipartite_flg = None


def is_bipartite(g):
    assert isinstance(g, Graph)

    global color, is_bipartite_flg
    color = dict()
    is_bipartite_flg = True

    for u in g.V:
        color[u] = 'white'

    for u in g.V:
        if color[u] == 'white':
            color[u] = 'red'
            is_bipartite_visit(g, u)
            if not is_bipartite_flg:
                return False
    return is_bipartite_flg


def is_bipartite_visit(g, u):
    global color, is_bipartite_flg

    if not is_bipartite_flg:
        return

    for v in g.get_neighbors(u):
        if color[v] == color[u]:
            is_bipartite_flg = False
            return
        elif color[v] == 'white':
            if color[u] == 'red':
                color[v] = 'blue'
            elif color[u] == 'blue':
                color[v] = 'red'
            is_bipartite_visit(g, v)

'''
# example run
g = Graph()
g.add_vertex('a0')
g.add_vertex('a1')
g.add_vertex('a2')
g.add_vertex('b0')
g.add_vertex('b1')
g.add_vertex('b2')
g.add_edge('a0', 'b0', directed_flg=False)
g.add_edge('a0', 'b1', directed_flg=False)
g.add_edge('a1', 'b1', directed_flg=False)
g.add_edge('a2', 'b2', directed_flg=False)
g.add_edge('a2', 'b1', directed_flg=False)

print is_bipartite(g)
# True

g.add_edge('b1', 'b2', directed_flg=False)
print is_bipartite(g)
# False

g.remove_edge('b1', 'b2', directed_flg=False)

# test if it works when g is not connected
g.add_vertex('c0')
g.add_vertex('c1')
g.add_vertex('d0')
g.add_vertex('d1')
g.add_edge('c0', 'd0', directed_flg=False)
g.add_edge('c0', 'd1', directed_flg=False)
g.add_edge('c1', 'd1', directed_flg=False)
print is_bipartite(g)
# True

g.add_edge('d0', 'd1', directed_flg=False)
print is_bipartite(g)
# False
'''