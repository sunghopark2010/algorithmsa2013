__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph
from GraphTheory.ClassMaterial.DAG import is_cyclic, topological_sort


def get_num_simple_paths_in_dag(dag, s, t):
    # check dag is a dag
    assert isinstance(dag, Graph)
    if is_cyclic(dag):  # O(V+E)
        raise ValueError('ERROR: given graph is cyclic.')

    # because graph is dag, find sink vertices; run dfs from sink vertices; mark vertices whose descendant is t
    # increment counter when meeting vertices which go to marked vertices

    gt = Graph.transpose(dag)
    l = topological_sort(gt)

    color = dict()
    num_simple_paths = dict()

    for u in dag.V:
        color[u] = 'white'
        num_simple_paths[u] = 0

    for u in l:
        if u == t:
            color[u] = 'red'
            num_simple_paths[u] = 1
        elif u == s:
            for v in dag.get_neighbors(u):
                if color[v] == 'red':
                    num_simple_paths[u] += num_simple_paths[v]
            break
        else:
            for v in dag.get_neighbors(u):
                if color[v] == 'red':
                    color[u] = 'red'
                    num_simple_paths[u] += num_simple_paths[v]

    return num_simple_paths[s]


# example run
dag = Graph()
dag.add_vertex('a')
dag.add_vertex('b')
dag.add_vertex('c')
dag.add_vertex('d')
dag.add_vertex('e')
dag.add_vertex('f')
dag.add_vertex('g')
dag.add_edge('a', 'c')
dag.add_edge('c', 'e')
dag.add_edge('c', 'f')
dag.add_edge('b', 'a')
dag.add_edge('d', 'c')
dag.add_edge('b', 'd')
dag.add_edge('e', 'g')
dag.add_edge('f', 'g')

print get_num_simple_paths_in_dag(dag, 'a', 'g')
# 2

print get_num_simple_paths_in_dag(dag, 'a', 'e')
# 1

print get_num_simple_paths_in_dag(dag, 'b', 'c')
# 2

print get_num_simple_paths_in_dag(dag, 'b', 'g')
# 4