__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph
from Common import INFINITY

q = None
d = None


def dijkstra_modified(g, w, s):
    assert isinstance(g, Graph)

    global d, q
    d = dict()
    q = list()
    n = dict()
    pi = dict()

    for u in g.V:
        d[u] = INFINITY
        n[u] = INFINITY
        pi[u] = None
        q.append(u)

    d[s] = 0
    n[s] = 0

    while q:
        u = _extract_min()

        for v in g.get_neighbors(u):
            if d[v] > d[u] + w(u, v):
                n[v] = n[u] + 1
                d[v] = d[u] + w(u, v)
                pi[v] = u
            elif d[v] == d[u] + w(u, v):
                if n[v] > n[u] + 1:
                    pi[v] = u
                    n[v] = n[u] + 1

    return d, pi, n


def _extract_min():
    global d, q
    u = filter(lambda vertex: d[vertex] == min(map(lambda fd: fd[1], filter(lambda distance: distance[0] in q, d.items()))), q)[0]
    q.remove(u)
    return u


# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_edge('a', 'b')
g.add_edge('b', 'c')
g.add_edge('c', 'd')
g.add_edge('a', 'e')
g.add_edge('e', 'd')


def weight_function(u, v):
    _weight_dict = dict()
    _weight_dict[('a', 'b')] = 1
    _weight_dict[('b', 'c')] = 1
    _weight_dict[('c', 'd')] = 2
    _weight_dict[('a', 'e')] = 2
    _weight_dict[('e', 'd')] = 2
    return _weight_dict[(u, v)]


d, pi, n = dijkstra_modified(g, weight_function, 'a')
print d
print pi
print n