__author__ = 'spark'

# a graph has an odd-length cycle if and only if one of its strongly connected components is not bi-partite.

from GraphTheory.ClassMaterial.Graph import Graph
from GraphTheory.ClassMaterial.SCC import dfs_scc
from GraphTheory.Homework.Homework6.Problem3 import is_bipartite


def has_odd_length_cycle(g):
    assert isinstance(g, Graph)
    scc = dfs_scc(g)

    for scc_index in set(scc.values()):
        g2 = _isolate_scc(g, scc, scc_index)
        if not is_bipartite(g2):
            return True
    return False


def _isolate_scc(g, scc, scc_index):
    gc = Graph()

    vertices_in_scc = map(lambda fascc: fascc[0], filter(lambda ascc: ascc[1] == scc_index, scc.items()))  # O(V)
    for u in vertices_in_scc:  # O(V)
        gc.add_vertex(u)

    for u in vertices_in_scc:  # O(V+E)
        for v in g.get_neighbors(u):
            if v in vertices_in_scc:
                gc.add_edge(u, v)

    return gc


# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_edge('a', 'b')
g.add_edge('b', 'c')
g.add_edge('c', 'a')
print has_odd_length_cycle(g)
# True

g.add_vertex('d')
g.remove_edge('c', 'a')
g.add_edge('c', 'd')
g.add_edge('d', 'a')
print has_odd_length_cycle(g)
# False

g.add_vertex('e')
g.add_vertex('f')
g.add_vertex('g')
g.add_edge('e', 'f')
g.add_edge('f', 'g')
g.add_edge('g', 'e')
g.add_edge('d', 'e')
print has_odd_length_cycle(g)
# True

g2 = Graph()
g2.add_vertex('A')
g2.add_vertex('B')
g2.add_vertex('C')
g2.add_vertex('D')
g2.add_vertex('E')
g2.add_edge('A', 'B')
g2.add_edge('B', 'C')
g2.add_edge('C', 'D')
g2.add_edge('D', 'E')
g2.add_edge('E', 'A')
print has_odd_length_cycle(g)
# True