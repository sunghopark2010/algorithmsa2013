__author__ = 'spark'

from GraphTheory.ClassMaterial.Graph import Graph
from GraphTheory.ClassMaterial.DAG import topological_sort
color = None


def is_semi_connected(g):
    # any graph is a DAG of its SCCs
    # find the source SCC
    # run DFS from a node in the source SCC
    # if DFS cannot reach all nodes from the node, then it is not semi-connected; otherwise, it is semi-connected

    assert isinstance(g, Graph)
    l = topological_sort(g)

    global color
    color = dict()
    for u in g.V:
        color[u] = 'white'
    s = l[0]
    color[s] = 'gray'

    for u in g.get_neighbors(s):
        _dfs_visit_semi_connected(g, u)

    for c in color.values():
        if c == 'white':
            return False
    return True


def _dfs_visit_semi_connected(g, u):
    global color
    color[u] = 'gray'
    for v in g.get_neighbors(u):
        if color[v] == 'white':
            _dfs_visit_semi_connected(g, v)
    color[u] = 'black'


# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_edge('a', 'b')
g.add_edge('a', 'c')
print is_semi_connected(g)
# True

g.add_vertex('d')
g.add_edge('d', 'c')
print is_semi_connected(g)
# False

g.remove_edge('d', 'c')
g.add_vertex('e')
g.add_vertex('f')
g.add_edge('e', 'f')
print is_semi_connected(g)
# False

g2 = Graph()
g2.add_vertex('a')
g2.add_vertex('b')
g2.add_vertex('c')
g2.add_vertex('d')
g2.add_vertex('e')
g2.add_vertex('f')
g2.add_edge('a', 'b')
g2.add_edge('b', 'c')
g2.add_edge('b', 'e', directed_flg=False)
g2.add_edge('c', 'f', directed_flg=False)
g2.add_edge('e', 'f')
g2.add_edge('b', 'd')
print is_semi_connected(g2)
# True
