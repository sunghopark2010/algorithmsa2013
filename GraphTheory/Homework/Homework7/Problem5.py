__author__ = 'spark'

# consists of two parts: finding path from v0 to u and from u to v0
# finding path from v0 to u is easy; just Dijkstra
# finding path from u to v0 involves an additional work; first do transpose, run Dijkstra
# when requested to find a path from u to w via v0, using the result from the second Dijkstra
# start from u and until hitting v0, show vertex and go up to parents
# from v0 to w, start from w, save vertices while going to their parent node while hitting v0
# reverse the result and how it

from GraphTheory.ClassMaterial.Graph import Graph
from GraphTheory.ClassMaterial.ShortestPath import dijkstra


class PathViaV0(object):
    def __init__(self, v0, to_v0_d, to_v0_pi, from_v0_d, from_v0_pi):
        self._v0 = v0
        self._to_v0_d = to_v0_d
        self._to_v0_pi = to_v0_pi
        self._from_v0_d = from_v0_d
        self._from_v0_pi = from_v0_pi

    def get_path_via_v0(self, u, w):
        result_path = list()

        uc = u
        result_path.append({'vertex': uc, 'distance': 0})

        while True:
            if uc == self._v0:
                break
            else:
                prev_dist = self._to_v0_d[uc]
                uc = self._to_v0_pi[uc]
                result_path.append({'vertex': uc, 'distance': prev_dist - self._to_v0_d[uc]})

        temp_result_path = list()

        uc = w
        while True:
            if uc == self._v0:
                break
            else:
                temp_result_path.append({'vertex': uc, 'distance': self._from_v0_d[w] - self._from_v0_d[self._from_v0_pi[w]]})
                uc = self._from_v0_pi[uc]

        temp_result_path.reverse()
        return result_path + temp_result_path


def find_shortest_paths_via_v0(g, w, v0):
    assert isinstance(g, Graph)
    from_d, from_pi = dijkstra(g, w, v0)

    gt = Graph.transpose(g)
    to_d, to_pi = dijkstra(gt, w, v0)

    return PathViaV0(v0, to_d, to_pi, from_d, from_pi)


# example run
g = Graph()
g.add_vertex('a')
g.add_vertex('b')
g.add_vertex('c')
g.add_vertex('d')
g.add_vertex('e')
g.add_vertex('f')
g.add_edge('a', 'b')
g.add_edge('a', 'c')
g.add_edge('b', 'd')
g.add_edge('b', 'e')
g.add_edge('c', 'f')
g.add_edge('c', 'd')
g.add_edge('d', 'e')
g.add_edge('d', 'f')
g.add_edge('f', 'e')
g.add_edge('e', 'a')


def weight_function(u, v):
    return 1

PathViaD = find_shortest_paths_via_v0(g, weight_function, 'd')
print PathViaD.get_path_via_v0('a', 'e')
# [{'distance': 0, 'vertex': 'a'}, {'distance': 1, 'vertex': 'b'}, {'distance': 1, 'vertex': 'd'}, {'distance': 1, 'vertex': 'e'}]
