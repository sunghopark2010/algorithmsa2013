__author__ = 'spark'

from Common import INFINITY

M = None


def find_fewest_number_of_coins(d, A):
    # You are given a list of n coin denomination values d[1...n] and an amount A.
    # You have an unlimited number of each type of coin. Your goal is to find a list of coins
    # that adds up to A, using the fewest number of coins possible.
    # Assume that there is always a denomination value 1 in d that A is a positive integer,
    # so the problem always has a solution. You can assume the denomination values
    # d[1], ..., d[n] are given in sorted order, i.e., 1 = d[1] < d[2] < ... < d[n].
    # Give an efficient dynamic programming algorithm to solve this problem.
    # Maximum points for most efficient algorithms.

    # I will use memoization

    # M is the DP, memoization table
    global M
    M = list()

    # find the largest index x such that d[x] < A
    x = len(d) - 1
    for i in range(0, len(d)):
        if d[i] > A:
            x = i-1
            break

    for i in range(0, A):
        if (i+1) in d:
            M.append(1)
        else:
            M.append(-1)

    return _find_fewest_number_of_coins(d, A, x)


def _find_fewest_number_of_coins(d, A, x):
    global M
    min_num_coins = INFINITY

    for i in range(x, -1, -1):
        num_coins = 0
        num_coins += (A / d[i])

        if A % d[i] > 0:
            if M[(A % d[i]) - 1] > 0:
                num_coins += M[(A % d[i]) - 1]
            else:
                next_x = x
                for j in range(0, len(d)):
                    if d[j] > (A % d[i]):
                        next_x = j-1
                        break
                num_coins += _find_fewest_number_of_coins(d, A % d[i], next_x)

        if num_coins < min_num_coins:
            min_num_coins = num_coins

    M[A-1] = min_num_coins
    return min_num_coins


# example run
print find_fewest_number_of_coins([1, 2, 3, 4, 6, 8], 15)
# 3
for i in range(20, 30):
    print '%d: %d' % (i, find_fewest_number_of_coins([1, 2, 3, 4, 6, 8], i))
    # 20: 3
    # 21: 4
    # 22: 3
    # 23: 4
    # 24: 3
    # 25: 4
    # 26: 4
    # 27: 4
    # 28: 4
    # 29: 5