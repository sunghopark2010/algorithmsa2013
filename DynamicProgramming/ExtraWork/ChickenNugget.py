__author__ = 'spark'


# Given M a list of number of chicken nuggets for menus, C a list of costs for each menu
# and n a number of chicken nuggets to purchase
# determine the number of purchases for each menu in such a way that reduces the sum of costs


def find_min_cost_combination(M, C, n):
    assert isinstance(M, list)
    assert isinstance(C, list)
    assert isinstance(n, int)
    assert len(M) == len(C)
    assert len(filter(lambda menu: menu <= 0, M)) == 0
    assert len(filter(lambda cost: cost <= 0, C)) == 0
    assert n >= 0

    return _find_min_cost_combination(M, C, n)


def _find_min_cost_combination(M, C, n):
    # construct a 2-D array whose size is len(M) * len(C)
    A = list()
    for i in range(0, len(M)):
        temp_list = list()
        for j in range(0, len(C)):
            temp_list.append(0)
        A.append(temp_list)

