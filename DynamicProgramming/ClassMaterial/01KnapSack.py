__author__ = 'spark'


def solve_01_knapsack(w, v, W):
    # given a list of weights and values
    # find a list of items such that their values are maximized while the sum of their weights is less than W

    M = list()
    for i in range(0, len(w)+1):
        temp_list = list()
        for j in range(0, W+1):
            temp_list.append(0)
        M.append(temp_list)

    for i in range(0, len(w)):
        M[i][0] = 0

    for j in range(0, W+1):
        M[0][j] = 0

    for i in range(1, len(w)+1):
        for j in range(1, W+1):
            if w[i-1] > j:
                M[i][j] = M[i-1][j]
            else:
                M[i][j] = max(M[i-1][j], M[i-1][j-w[i-1]]+v[i-1])

    return M


print solve_01_knapsack([1, 2, 3], [6, 10, 12], 5)