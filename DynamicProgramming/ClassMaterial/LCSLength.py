__author__ = 'spark'


def get_LCS_length(X, Y):
    # given two sequences X and Y
    # find the longest subsequence common to both X and Y

    # c is the DP table
    c = list()
    for i in range(0, len(X)+1):
        temp_table = list()
        for j in range(0, len(Y)+1):
            temp_table.append(0)
        c.append(temp_table)

    for i in range(0, len(X)+1):
        c[i][0] = 0

    for j in range(0, len(Y)+1):
        c[0][j] = 0

    for i in range(1, len(X)+1):
        for j in range(1, len(Y)+1):
            if X[i-1] == Y[j-1]:
                c[i][j] = 1 + c[i-1][j-1]
            else:
                c[i][j] = max(c[i-1][j], c[i][j-1])

    return c[-1][-1]


print get_LCS_length('ABCBDAB', 'BDCABA')
