__author__ = 'spark'


def find_contig_max_sum(A):
    # Given an array of A[1, ..., n] of n real numbers
    # find a contiguous subsequence of maximum sum

    # M is the DP table
    M = list()
    M.append(0)

    seq_start = 0
    seq_end = 0
    best_sum = 0
    current_start = 0

    for i in range(1, len(A)+1):
        if M[i-1] >= 0:
            M.append(M[i-1] + A[i-1])
        else:
            M.append(A[i-1])
            current_start = i-1
        if M[i] > best_sum:
            best_sum = M[i]
            seq_start=current_start
            seq_end=i-1

    return seq_start, seq_end, best_sum


# example run
print find_contig_max_sum([10, -5, 40, 10, -30, 15, 5])