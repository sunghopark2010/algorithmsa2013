__author__ = 'spark'


def find_max_revenue_in_rod_cutting(P, n):
    # given an integer n(length) and array of prices P[1 ... n]
    # find the maximum revenue obtainable for rods whose lengths sum to n

    # R is the DP table
    R = list()

    for i in range(0, len(P)):
        max_revenue_from_cut = 0
        for j in range(0, i):
            if R[i-j-1] + R[j] > max_revenue_from_cut:
                max_revenue_from_cut = R[i-j-1] + R[j]
        if P[i] < max_revenue_from_cut:
            R.append(max_revenue_from_cut)
        else:
            R.append(P[i])

    return R


# example run
find_max_revenue_in_rod_cutting([1, 5, 8, 9, 10, 17, 17, 20], 7)