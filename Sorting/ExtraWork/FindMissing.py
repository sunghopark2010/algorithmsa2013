__author__ = 'spark'

import math


def find_missing(A):
    # Given a sorted array A containing N - 1 distinct integers,
    # each in the range {1, ..., N}
    # Find the number that is not in A

    return _find_missing(A, 0, len(A)-1)


def _find_missing(A, p, r):
    if p < r:
        mid_point = int(math.floor((p + r) / 2))
        if A[mid_point] == mid_point+1:  # the left subarray has all numbers
            return _find_missing(A, mid_point+1, r)
        elif A[mid_point] > mid_point+1:  # the left subarray misses a number
            return _find_missing(A, p, mid_point-1)
        # the other case A[mid_point] < mid_point is not possible
        # because the left subarray either has all numbers or misses one number
        # both of whose cases are covered
    else: # if p and r are same, the missing number exists right next to p
        return p + 1


# example run
print find_missing([1, 2, 3, 4, 6, 7, 8, 9])
# 5

print find_missing([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16])
# 15