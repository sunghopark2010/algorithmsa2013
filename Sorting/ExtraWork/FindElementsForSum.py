__author__ = 'spark'


def find_elements_for_sum(A, n):
    # given sorted array A whose elements are real numbers
    # find if two elements sum to a real number n
    # if not, return None
    # running time requirement: O(n)

    assert isinstance(A, list)
    assert isinstance(n, int)

    # checking A is sorted takes at least O(nlgn) so did not check
    return _find_elements_for_sum(A, n)


def _find_elements_for_sum(A, n):
    if not A:
        return None

    if A[0] + A[-1] > n:
        A.remove(A[-1])
        return _find_elements_for_sum(A, n)

    if A[0] + A[-1] < n:
        A.remove(A[0])
        return _find_elements_for_sum(A, n)

    if A[0] + A[-1] == n:
        return (A[0], A[-1])


# sample run
print find_elements_for_sum([1, 2, 3, 4, 5, 6, 8, 9], 14)
# (5, 9)
print find_elements_for_sum([1, 2, 3, 4, 5, 6, 8, 9], 100)
# None

print find_elements_for_sum([-4, -2, -1, 1, 5, 6, 8, 9], -1)
# None
