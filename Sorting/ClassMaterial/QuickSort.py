__author__ = 'spark'


def quick_sort(A, p=0, r=None):
    # running time: O(nlogn)
    # sort in place: i.e. does not require external lists

    assert isinstance(A, list)
    assert isinstance(p, int)
    if not r:
        r = len(A) - 1
    assert isinstance(r, int)

    return _quick_sort(A, p, r)


def _quick_sort(A, p, r):
    if p < r:
        q = partition(A, p, r)
        _quick_sort(A, p, q-1)
        _quick_sort(A, q+1, r)


def partition(A, p, r):
    x = A[r]
    i = p - 1

    for j in range(p, r+1):
        if A[j] < x:
            i += 1
            a = A[i]
            b = A[j]
            A[i] = b
            A[j] = a

    a = A[i+1]
    A[r] = a
    A[i+1] = x
    return i+1


# example run
A = [5, 3, 4, 9, 1, 6]
print A
# [5, 3, 4, 9, 1, 6]
quick_sort(A, 0, 5)
print A
# [1, 3, 4, 5, 6, 9]