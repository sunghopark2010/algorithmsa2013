__author__ = 'spark'


def insert_sort(A):
    # Running time: O(n^2)

    assert isinstance(A, list)
    B = list()
    map(lambda element: B.append(element), A)

    n = len(B)

    for j in range(1, n):
        key = B[j]
        i = j - 1
        while i >= 0 and B[i] > key:
            B[i+1] = B[i]
            i -= 1

        B[i+1] = key

    return B


# example run
A = [5, 3, 4, 9, 1, 6]
print insert_sort(A)
# [1, 3, 4, 5, 6, 9]