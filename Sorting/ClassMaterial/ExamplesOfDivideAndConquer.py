__author__ = 'spark'

import math


def fast_power(a, n):
    assert isinstance(a, int)
    assert isinstance(n, int)
    return _fast_power(a, n)


def _fast_power(a, n):
    if n == 1:
        return a
    else:
        x = _fast_power(a, int(math.floor(n/2)))
        if n % 2 == 0:
            return x * x
        else:
            return x * x * a

print "fast_power(2, 10) ="
print fast_power(2, 10)


def fast_multiply(x, y):
    assert isinstance(x, int)
    assert isinstance(y, int)

    return _fast_multiply(x, y)


def _get_digits(n, q):
    divisor = int(math.pow(10, q))
    n0 = n / divisor
    n1 = n % divisor
    return n0, n1


def _fast_multiply(x, y):
    if x == 0 or y == 0:
        return 0

    n1 = int(math.log(x, 10))
    n2 = int(math.log(y, 10))
    n = max(n1, n2)

    if n == 0:
        # what I really should have done here is a table lookup
        return x * y
    else:
        q = n - n/2
        x0, x1 = _get_digits(x, q)
        y0, y1 = _get_digits(y, q)
        p1 = _fast_multiply(x0, y0)
        p2 = _fast_multiply(x1, y1)
        p3 = _fast_multiply(x0+x1, y0+y1)
        # print 'x: %d, y: %d' % (x, y)
        # print 'n: %d, q: %d' % (n, q)
        # print 'x0: %d, x1: %d, y0: %d, y1: %d' % (x0, x1, y0, y1)
        # print 'p1: %d, p2: %d, p3: %d' % (p1, p2, p3)
        # print 'result: %d' % int(math.pow(10, n) * p1 + math.pow(10, q) * (p3 - p1 - p2) + p2)
        if n > q:
            return int(math.pow(10, n) * p1 + math.pow(10, q) * (p3 - p1 - p2) + p2)
        else:
            return int(math.pow(10, 2 * n) * p1 + math.pow(10, q) * (p3 - p1 - p2) + p2)

# example run
print fast_multiply(100, 12)
# 1200
print fast_multiply(10, 12)
# 120
print fast_multiply(123, 234)
# 28782