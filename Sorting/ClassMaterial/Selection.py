__author__ = 'spark'

from Sorting.ClassMaterial.QuickSort import partition


def select(A, p=0, r=None, i=1):
    assert isinstance(A, list)
    assert isinstance(p, int)
    if not r:
        r = len(A) - 1
    assert isinstance(r, int)
    assert isinstance(i, int)
    assert 1 <= i <= len(A)

    return _select(A, p, r, i)


def _select(A, p, r, i):
    if p == r:
        return A[p]
    q = partition(A, p, r)
    k = (q - p + 1)
    if i == k:
        return A[q]
    elif i < k:
        return _select(A, p, q-1, i)
    else:
        return _select(A, q+1, r, i-k)


# example run
A = [5, 3, 4, 9, 1, 6]
for i in range(1, len(A)+1):
    print '%d smallest number: %d' % (i, select(A, i=i))
    # 1 smallest number: 1
    # 2 smallest number: 3
    # 3 smallest number: 4
    # 4 smallest number: 5
    # 5 smallest number: 6
    # 6 smallest number: 9