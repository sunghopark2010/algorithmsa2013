__author__ = 'spark'

import math
from Common import INFINITY


def merge_sort(A, p=0, r=None):
    # Divide & Conquer method
    # Von Neumann (1945)

    # A: list of numbers to be sorted
    # p: left delimiter
    # r: right delimiter

    assert isinstance(A, list)
    assert isinstance(p, int)
    if not r:
        r = len(A) - 1
    assert isinstance(r, int)

    _merge_sort(A, p, r)


def _merge_sort(A, p, r):
    if p < r:
        q = int(math.floor((p + r) / 2))
        _merge_sort(A, p, q)
        _merge_sort(A, q+1, r)
        _merge(A, p, q, r)


def _merge(A, p, q, r):
    L = list()
    R = list()
    for i in range(p, q+1):
        L.append(A[i])
    for i in range(q+1, r+1):
        R.append(A[i])
    L.append(INFINITY)
    R.append(INFINITY)

    index_l = 0
    index_r = 0

    for k in range(p, r+1):
        if L[index_l] > R[index_r]:
            A[k] = R[index_r]
            index_r += 1
        else:
            A[k] = L[index_l]
            index_l += 1


# example run
A = [5, 3, 4, 9, 1, 6]
print A
# [5, 3, 4, 9, 1, 6]
merge_sort(A, 0, 5)
print A
# [1, 3, 4, 5, 6, 9]